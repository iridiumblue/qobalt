function onError(error) {
  alert(error);
}

function save(e) {
   var val = "";
   var val2 = "";

   d_classic = document.getElementById('classic').checked;

   if (document.getElementById('radio1').checked) {
      val = "highlight";
   } else if (document.getElementById('radio2').checked) {
      val = "suppressed";
   } else {
      val = "chill";
   }

   if (document.getElementById('radio2_1').checked) {
      val2 = "highlight";
   } else if (document.getElementById('radio2_2').checked) {
      val2 = "hide";
   } else {
      val2 = "show";
   }

    console.log("Tryin!! " + val + " " + val2);

  chrome.storage.local.set({
    mode: val,
    anon: val2,
    classic_view: d_classic,
    link_color : document.getElementById('link_color').value,
    anon_color : document.getElementById('anon_color').value,
    theme_color : document.getElementById('theme_color').value
  });
  
  console.log("Saved!");

  e.preventDefault();
}

function restoreOptions() {
  
  store = chrome.storage.local.get(null,function(res){
      var mode = res["mode"];
      var anon = res["anon"];
      var link_color = res["link_color"];
      var anon_color = res["anon_color"];
      var theme_color = res["theme_color"]
      var classic = res["classic_view"];

      if (typeof(mode)=='undefined') {mode='highlight';}
      if (typeof(anon)=='undefined') {anon='highlight';}
      if (typeof(theme_color) == 'undefined') {theme_color = '#cccccc';}
      if (typeof(link_color)=='undefined') {link_color='#ffffcc';}
      if (typeof(anon_color)=='undefined') {anon_color='#e7f4e6';}
      if (typeof(classic) == 'undefined') {classic = false;}
      if (mode == 'highlight') {
            document.getElementById('radio1').checked = true;
      } else if (mode == 'suppressed') {
            document.getElementById('radio2').checked = true;
      } else {
            document.getElementById('radio3').checked = true;
      }

      if (anon == 'highlight') {
            document.getElementById('radio2_1').checked = true;
      } else if (anon == 'hide') {
            document.getElementById('radio2_2').checked = true;
      } else {
            document.getElementById('radio2_3').checked = true;
      }
      d_link = document.getElementById('link_color');
      d_anon = document.getElementById('anon_color');
      d_theme = document.getElementById('theme_color');
      d_classic = document.getElementById('classic');
      d_classic.checked = classic;


      d_link.value=link_color;
      d_anon.value=anon_color;
      d_theme.value=theme_color;
      d_anon.addEventListener('change', save);
      d_link.addEventListener('change', save);
      d_theme.addEventListener('change', save);
      d_classic.addEventListener('change', save);
      //document.getElementById('radio1').addEventListener('change', save);
      bts = document.getElementsByClassName('rbutton');
      for (i=0; i<bts.length; i++) {
         bts[i].addEventListener('change', save);
      }
  });

}

//document.getElementById('save').addEventListener('click',save);
document.addEventListener('DOMContentLoaded', restoreOptions);

document.addEventListener('DOMContentLoaded', restoreOptions);
