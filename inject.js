
var FIREFOX=false;


QOBALT_VERSION = 40;

g_items = []; // persists
g_items_set = new Set(); // persists ensure uniqueness 
g_anon_color='';
g_profile_link='';



SNAP_MINUTES = 12*60;  // minutes
REFRESH_INTERVAL = 30 ; //minutes

NOTIFICATION_INTERVAL = 5; // minutes

MAX_CONTENTS=80;

TAB2_PAUSE_FOR_ANSWER = 500;


FAST_MODE="production";

if (FAST_MODE=="fast") {
   REFRESH_INTERVAL = 5;
   MAX_CONTENTS=5;
   TAB2_PAUSE_FOR_ANSWER=2000;
   TAB2_WAIT_FOR_SCROLL=10000;
} else if (FAST_MODE=="medium") {
   REFRESH_INTERVAL = 30;
   MAX_CONTENTS=30;
   TAB2_PAUSE_FOR_ANSWER=10000;
   //TAB2_WAIT_FOR_SCROLL=10000;
}

//REFRESH_INTERVAL = REFRESH_INTERVAL*60; //seconds


var g_working=false;

var g_background_color="#777777";

function work_off()
{
  var xx = document.getElementById('qobalt_overlay');
  var store = chrome.storage.local.get(null,function(res){
     if (typeof(res['progress_num']!='undefined')) {
       dpr = document.getElementById('mybar');
       dpr.style.width = (100.0*res['progress_num'])/MAX_CONTENTS + '%'; 
     }

  });
  
  if (xx==null) {
   
    return;
  }
  
  xx.style.opacity = 0.85;
  if (g_working) {
    setTimeout(function(){work_on();},550);
  } else {
    ; // reset
  }
}


function work_on()
{
  var xx = document.getElementById('qobalt_overlay');
  if (xx==null) {  
    return;
  }
  
  xx.style.opacity = 0;
  setTimeout(function(){work_off();},1500);
}

function start_work(mode=true)
{

  g_working = mode;
  pbar = document.getElementById("myProgress");
  wp = document.getElementById("wait_prompt");
  up = document.getElementById("update_prompt");
  if (g_working) {

     wp.style.display="none";
     pbar.style.display="block";
     up.style.display="block";
     storage = chrome.storage.local.set({
           "progress_num": 0
     },function(){
       work_on();
     });
     
     
  } else {
     pbar.style.display="none";
     up.style.display="none";
     wp.style.display="block";
     chrome.storage.local.set({
           "progress_num": 0
     });
  }
}

function skippast(txt,token)
{
  var pos = txt.indexOf(token);
  //console.log('~~~ skippast ' + token + ' ' + pos);
  if (pos<0) {
     console.log("skippast : error locating " + token);
     return '';
  }
  pos += token.length;
  return txt.substr(pos);
}

function scanto(txt,token,lastone = false) 
{
  var pos = 0;
  if (!lastone) {
    pos = txt.indexOf(token)
  } else {
    pos = txt.lastIndexOf(token);
  }
  if (pos<0) {
     console.log("scanto : error locating " + token);
     return '';
  }
  return txt.substr(0,pos);


}

function fade(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}

function showBanner()
{
    return;
    var div = document.createElement('div');
    div.id = 'qure_id';
    div.style.width = '100%';
    div.style.height='15px';
    div.style.backgroundColor = '#444444';
    div.style.color = 'white';
    div.style.paddingTop = '5px';
    div.style.paddingBottom = '5px';
    div.style.textAlign='center';
    div.innerHTML = 'Thank you for trying the Beta pre-release of Qobalt!</a>';
    var header = document.getElementsByClassName('SiteHeader')[0];
    header.appendChild(div);
    setTimeout(function(){ fade(div); }, 10000);
}

function wait_for_it(el)
{
   el = skippast(el,"PagedListMoreButton");
   //console.log(el); 
}



function getAsker(qhref)
{
   //console.log(qhref + "/log");
   var myFetch = fetch(qhref + "/log");

   myFetch.then(function(response) {
        response.text().then(function(text) {
          try {
            var url = response.url;
            
            url = scanto(url,'/log');
            ancestor = window.qlinks_seen[url];
            //console.log(text);
           
            text=skippast(text,'Question Log');
             
            text = skippast(text,'Question added by');
            if (text=='') {
               asker = '<span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by ??</span>';
            } else if (text.indexOf('Anonymous')>-1) {
               if (window.anon == 'highlight') {
                    ancestor.style.backgroundColor=g_anon_color;
               } else {
                    ancestor.style.height=0;
                    ancestor.style.display="none";
               }

               asker = '<span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by Anonymous</span>';
               
            } else {
               text = skippast(text,"<a class='user' href='");
               text = scanto(text,"'");
               
               uname = skippast(text,"/profile/").replace(/-/g,' ');
               asker = '</a><span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by <a href="/profile/' + text + '">' + uname + '</a></span>';
            }
               
            qtext = ancestor.querySelector('.ui_qtext_rendered_qtext');
            qtext.outerHTML = qtext.outerHTML + asker;
            
          } catch (err) {
             console.log(err);
            
          }
      });
  });
}

function scanForAnonQ()
{

    var questions = document.getElementsByClassName("question_link");
    var i=0;
    for (i=0; i<questions.length; i++) {
       var ancestor = questions[i].closest('.multifeed_bundle_story');
       if (ancestor == null) continue;

       var href = questions[i].href;
       
       if (!(href in window.qlinks_seen)) {
          //console.log(href);
          window.qlinks_seen[href] = ancestor;
          getAsker(href);
       }
    }

}

// https://stackoverflow.com/a/14570614

var observeDOM = (function(){
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
        eventListenerSupported = window.addEventListener;

    return function(obj, callback){
        if( MutationObserver ){
            // define a new observer
            var obs = new MutationObserver(function(mutations, observer){
                if( mutations[0].addedNodes.length || mutations[0].removedNodes.length )
                    callback();
            });
            // have the observer observe foo for changes in children
            obs.observe( obj, { childList:true, subtree:true });
        }
        else if( eventListenerSupported ){
            obj.addEventListener('DOMNodeInserted', callback, false);
            obj.addEventListener('DOMNodeRemoved', callback, false);
        }
    };
})();

/*
observeDOM( el,function(){ 
    console.log('XXXXXXXXXXXXXXXXXXXXXXXX ---   dom changed');
});
*/

function doAnswer(message) {
  console.log(`Message from the background script:  ${message.response}`);
}

function handleError(error) {
  console.log(`Error: ${error}`);
}

var g_cur_item = 0;
var g_cur_url="";


function starThis(val)
{
  ret = "<div style='font-size:11px; position:absolute; width:23px; line-height: 23px; height:23px; top: 5px; left:0px; border-radius:3px; background-image: url(\"" + chrome.extension.getURL("images/star.png") + "\")'> <div style='position:absolute; top:2px; left:0px; width:23px; text-align:center'> " + val + "</div></div>";
  return ret;
}



function arrowThat(val, left=false)
{
  if (!left) {
    ret = "<div style='width:40px; margin-top:7px; line-height:18px; height:18px; font-size:11px; position:absolute; vertical-align: middle; left:315px; border-radius:3px; background-image: url(\"" + chrome.extension.getURL("images/nnuparrow.png") + "\")'><span style='position:absolute; width:32px; text-align:right;left:4px; top:3px;'>" + val + "</span></div>";
  } else {
    ret = "<div style='width:36px; margin-top:-4px; line-height:18px; height:18px; font-size:11px; position:absolute; vertical-align: middle; left:0px; border-radius:3px; background-image: url(\"" + chrome.extension.getURL("images/nnuparrow.png") + "\")'><span style='position:absolute; width:28px; text-align:right;left:4px; top:3px;'>" + val + "</span></div>";
 
  }
   return ret;
  
}


function shortenString(item)
{
  var line="";

  if (item.length > 84) {
           item = item.substr(0,42) + " ... " + item.slice(-42);
  }
  /*

vertical-align: middle;
line-height: 90px;  
  */
  //line = line + starThis(3);
  line = line + item + "<br>";
  //line = line + '<div style="height:1px; overflow:hidden; width=70%; background-color:#aaaaaa;">&nbsp;</div>';        
  return line;

}

g_panel = null;

function fireNewStats()
{
   g_cur_item=0;
   var init_progress = chrome.storage.local.set({
        "progress": "pending"
   }, function(){
      chrome.runtime.sendMessage({name: "refresh_stats"});  
      chrome.runtime.sendMessage({name: "open_comment_tab"}); // guaranteed to finish first, so timing ignored.
      setTimeout(surfaceFeedUpvotes,500);
   }); 
   start_work(true);

   
}

g_warp_refreshTimer=false;

function updateMinutes(count)
{
    mn = count/60+1;
    mn = parseInt(mn.toString());
    wt = document.getElementById('wait_min');
    wt.innerHTML = mn;
}

function startRefreshTimer(count)
{
   if (g_warp_refreshTimer) {
   	  //alert("Forcing stage C");
      g_warp_refreshTimer=false;
      count=-1;  
   }
   if (count<0) {
     fireNewStats();
     return;
   }
   updateMinutes(count);
   console.log("TIMER : " + count);
   setTimeout(function(){startRefreshTimer(count-1)},1000);
}

function cullSnapshots(snapshots) 
{
   try {
     // remove all but youngest snapshot older than SNAP_MINUTES
     var cur_stamp = (new Date).getTime()/1000/60; // to minutes
     
     for (i=0; i<snapshots.length; i++) {
        snap = snapshots[i];
        tm = snap['tstamp'];   
        if ( (cur_stamp-tm) > SNAP_MINUTES) {
          for (j=i+1; j<snapshots.length; j++) {
             snapshots.shift();
          }
          return snapshots;
        }
     }
     return snapshots;
   } catch(err) {
    console.log(err);
   }
}

function updateSnapshotsAndDeltas(res,upvotes)
{
   try {
      if (!('snapshots' in res)) {
        res['snapshots'] = []
      }
      res['snapshots'] = cullSnapshots(res['snapshots']);
      var upvotes = res['upvotes'];
      var tstamp = (new Date).getTime()/1000/60; // to minutes
      var snap = {tstamp: tstamp, upvotes: upvotes};
      res['snapshots'].push(snap);
      var save = chrome.storage.local.set({
           'snapshots': res['snapshots']
      });
      //
      //... update deltas
      //
      if (res['snapshots'].length>0) {
         osnaps = res['snapshots'][0]['upvotes'];
         snap_time = res['snapshots'][0]['tstamp'];
      } else {
         osnaps = {};
      }

      
      mkeys = Object.keys(upvotes);
      for (i=0; i<mkeys.length;i++) {
        url = mkeys[i];
        
        if (url in osnaps) {
          upvotes[url]['delta'] = upvotes[url]['upvotes'] - osnaps[url]['upvotes'];
        } else {
          upvotes[url]['delta']=upvotes[url]['upvotes'];
        }

      }
      
      return upvotes;
  } catch(err) {
     console.log(err);
  }      
}

function panelSkeleton()
{
  var panel = document.getElementsByClassName('home_feed_right_col')[0];
  panel = panel.getElementsByClassName('fixable_clone')[0];
  panel.onmouseover = function(e) {
     dd = document.getElementById('qobalt_donate');
     dd.style.transition = "opacity 0.5s";
     dd.style.opacity = 1.0;

    //alert("Mouse in");
  };
  panel.onmouseout = function(e) {
     dd = document.getElementById('qobalt_donate');
     dd.style.opacity = 0.0;

     //document.getElementById('qobalt_donate').style.display='0.0';
  };

  //panel = panel.children[0];
  g_panel = panel;
  panel.style.width="390px";
  sep = '<div style="height:1px; overflow:hidden; margin-bottom:10px; width:350px; background-color:white;">&nbsp;</div>';

  list="";
      // float:right; margin-right:40px; margin-top:-25px; border-radius:3px; max-width:70px; width:70px; height:48px;
  list = list + "<a target='_blank' href='https://www.paypal.me/creiss/10' style='float:left; margin-left: 2px; margin-top: -11px;'><div id = 'qobalt_donate' style='height:50px; width:50px; opacity:0.0 !important;' ><img width=40 height=40 style='margin-left: 5px;' src='" + chrome.extension.getURL("images/donate.png") + "'><br>Donate!</div></a>";

  list= list + "<a href='#' id=\"qobalt_logo\" style=\"float:right; margin-right:40px; outline:none; margin-top:-25px; border-radius:3px; max-width:70px; width:70px; height:48px; background: url('" + chrome.extension.getURL("images/qobalt.png") + "');\" id='qobalt-bg'>";
  list= list + "<div style='width:70px; height:48px;background-color: #cccccc;transition: opacity 0.40s; opacity : 0.5;' id='qobalt_overlay'>";
  list= list + "&nbsp;";
  list= list + "</div>";
  list= list + '<div id="wait_prompt" style="margin-top:3px; margin-left:-27px; font-size:8px; color:#666666">Next update in <span id="wait_min"></span> min.<br>Click to fire now.</div>';
  list= list + "<div style='width: 70px; margin-top:4px; background-color:grey;' id='myProgress'><div style='width: 1%; height: 4px;color:#444444; background-color: #ffffcc; text-align:left; padding-left:2px;' id='mybar'></div></div>";
  list= list + '<div id="update_prompt" style="display:none;margin-top:3px; margin-left:-8px; font-size:8px; color:#666666">Update in progress</div>';
  list = list + "</a>";
  //...............................................
  list = list + "<div style='height:65px;'>&nbsp;</div>"

  list = list + "<img style='border-radius:3px;' height=20 width=20 src='" + chrome.extension.getURL("images/trending.png") + "'>";
  // The Jim Gordon clause - he hates the word 'trending'

  if (g_profile_link.indexOf('Jim-Gordon')>0) {
    list = list + "&nbsp;<b>Recent answers getting upvotes<b>";
  } else {
    list = list + "&nbsp;<b>My Trending Answers</b> - recent answers getting upvotes";
  }

  list = list + sep;
  list = list + "<img id='prompt1' style='display:none;float:left; margin-left:10px; margin-bottom:-5px; border-radius:3px;height:20px;' src='" + chrome.extension.getURL("images/prompt1.png") + "'>";
  list = list + '<div id="prompt2" style="display:none;float:left; padding-bottom:5px;height:20px; color:white"><b>Upvotes last 12 hours</b><br>* This takes half a day after install to get interesting.</div>';
  list = list + '<div style="height:29px;">&nbsp;</div>';
  list = list + "<div id='qobalt_trending'></div>"; 
  
  //...............................................  

  list = list + "<img style='margin-top:15px; border-radius:3px;' height=20 width=25 src='" + chrome.extension.getURL("images/sunrise.png") + "'>";

  list = list + "&nbsp;<b>My Latest Answers</b>";
  list = list + sep;  
   
  list = list + "<div id='qobalt_recent'></div>";  
  //...............................................
  list = list + "<img style='margin-top:15px; border-radius:3px;' height=20 width=25 src='" + chrome.extension.getURL("images/postit.png") + "'>";

  list = list + "&nbsp;<b>My Latest Comments</b>";
  list = list + sep;  
   
  list = list + "<div style='margin-top:5px;' id='qobalt_comments'></div>";  

  //...............................................  
  list = list + "<img style='margin-top:15px; border-radius:3px;' height=20 width=20 src='" + chrome.extension.getURL("images/sbell.png") + "'>";
  list = list + "&nbsp;<b>Notifications</b>";
  list = list + sep;

  list = list + "<div id='qobalt_notifications' style='margin-left:15px; width:310px; height:800px'></div>";
  panel.innerHTML = list;
}

g_previous_trending=null;
//g_previous_comments=null;

function flashDivs(div_list)
{
  for (i=0; i<div_list.length; i++) {
    panel = document.getElementById(div_list[i]);
    pmt = panel.getElementsByClassName('flash_prompt')[0];
    pmt.style.display = "block";
    panel.style.backgroundColor = "#E6E6E6";
  }
  
}


function hoverDiv(obj) 
{
    //obj = event.target || event.srcElement;
    
    obj.style.background = "linear-gradient(to right, #FFF 0%, #888 100%);";
}

     

function addEvents()
{
  try {
    xx=document.getElementById('qobalt_logo');
    xx.addEventListener("click", function(event){ 
      event.stopPropagation(); 
      forceUpdate();        
    });
  } catch(err) {
    console.log(err);
  }
  /*
  try {
    cl1 = "quark_trending_divs";
    divs = document.getElementsByClassName(cl1);
    for (i=0; i<divs.length; i++) {
       xx = divs[i];
       xx.addEventListener("mouseover", function(event){ 
          event.stopPropagation(); 
          
          obj = event.target;
          obj["oldBackground"] = obj.style.backgroundColor;
          obj.style.backgroundColor = "#FFFFCC";
          
        });
       xx.addEventListener("mouseout", function(event){ 
          event.stopPropagation(); 
         
          obj = event.target;
          obj.style.backgroundColor = obj["oldBackground"];
          
        });

       //xx.onclick = function () {hoverDiv(xx);}
    };
  } catch(err) {
    ;
  }
  */
}

function timeOfLastUpdateThen()
{
  try{
   var store = chrome.storage.local.get(null,function(res) {
     if (('snapshots' in res) && ('progress' in res) && (res['progress']=='ready')) {
       snapshots = res['snapshots'];
       last = snapshots[snapshots.length-1];
       dstamp = last['tstamp'];

       tstamp = (new Date).getTime()/1000/60; //minutes
       
       age = tstamp - dstamp;
     } else {
       age = REFRESH_INTERVAL+1; // force update
     }

     if (age>REFRESH_INTERVAL) {
       fireNewStats();
       //startRefreshTimer(1);
     } else {
       
       quicker = REFRESH_INTERVAL - age;
       quicker = parseInt(quicker.toString());
       start_work(false);
       startRefreshTimer(quicker*60);
      
     }
    
   });
   
 } catch(err) {
   console.log(err);
 }
  //poom
}

function iconHelp(show=false)
{
	xx=document.getElementById('prompt1');
	xy=document.getElementById('prompt2');
	if (show) {
		st = 'block';
	} else {
		st='none';
	}
	xy.style.display=st;
	xx.style.display=st;

}


function surfaceFeedUpvotes(rightNow=false)
{     
   divs_to_flash=[];
   var store = chrome.storage.local.get(null,function(res){
    try {
      var upvotes = res["upvotes"];
      var progress = res["progress"];

      if (rightNow) {
        if (!('upvotes' in res) || !('progress' in res) || (res['progress']!='ready')) {
            console.log("No data yet.")
            //startRefreshTimer(REFRESH_INTERVAL);
            return;
        }
      }
      
      if (!(rightNow)) {
         if (progress!='ready') {
           setTimeout(surfaceFeedUpvotes,500);
           return;
         } else {
           start_work(false);
           startRefreshTimer(REFRESH_INTERVAL*60);
         }
      }
      iconHelp(true);
      upvotes = updateSnapshotsAndDeltas(res, upvotes);
      mkeys = Object.keys(upvotes);
      console.log("PREVIOUS --- : " + JSON.stringify(g_previous_trending));
      console.log('--------------------\n');
      console.log("CURRENT -- " + JSON.stringify(upvotes));
      if (g_previous_trending!=null) {
        for (i=0; i<mkeys.length;i++) {
          url = mkeys[i];
          if ((!(url in g_previous_trending) || (g_previous_trending[url]["upvotes"]!=upvotes[url]["upvotes"])) && (upvotes[url]["delta"]>0)){
            upvotes[url]["flash"]=true;
          } else {
            upvotes[url]["flash"]=false;
          }
     
        }
      }


      g_previous_trending = JSON.parse(JSON.stringify(upvotes));  // Deep copy.  Fuck you, Javascript.  https://stackoverflow.com/questions/122102/what-is-the-most-efficient-way-to-deep-clone-an-object-in-javascript

      mkeys.sort(function(a, b){
         return (upvotes[a]["order"] - upvotes[b]["order"]);
      });
      list="";

      for (i=0; i<5; i++) {
         urls = mkeys[i];

         upv = upvotes[urls];
         label = upv["label"];
         
         list = list + '<div style="position:relative; margin-left:5px; width:330px; margin-top:7px; min-height:20px; margin-bottom:3px;">';
         list = list + arrowThat(upv["upvotes"], true) + '<a style="color:black;" target="_blank" href="' + urls + '"><div style="margin-left:44px; left:24px; top:7px; width:290px;">' + shortenString(label) + "</div></a>";
         /* 
         if (i<5-1) {
            list = list + '<div style="position:absolute; top:43px; margin-bottom:4px; position:relative; left:25px; width:331px; height:1px; background-color:#AAAAAA;">&nbsp;</div>';
         }
         */
         list = list + "</div>";

         //list = list + shortenString(upv["upvotes"] + " " + label);

      }
      // Add comments
      xx = document.getElementById('qobalt_recent');
      xx.innerHTML = list;
      
      setTimeout(function(){flashDivs(divs_to_flash)},700);
      //..............
      list="";
      try {
      comments = res["comments"];
        for (i=0; i<comments.length;i++) {
           list = list + '<div style="margin-left:15px;  margin-bottom:5px; width:300px; color:#555555"><i>' + comments[i]['question'] + '</i></div>';
           list = list + '<a style="color:black;" target="_blank" href="' + comments[i]['url'] +  '"><div style="padding-bottom:7px; margin-left:38px; width:300px;">' + shortenString(comments[i]['txt']) + '</div></a>';          
        }
        xx = document.getElementById('qobalt_comments');
        xx.innerHTML = list;
      } catch(err) {
         ;
      }

      //..............

      list="";
      
      mkeys.sort(function(a, b){
         if (upvotes[b]["delta"] == upvotes[a]["delta"]) {
           return upvotes[b]["upvotes"] - upvotes[a]["upvotes"];
         } else {
           return upvotes[b]["delta"] - upvotes[a]["delta"];
         }
      });

      for (i = 0; i<mkeys.length; i++) {
         id_name = "qobalt_trending_id_" + i;
         
         urls = mkeys[i];
         upv = upvotes[urls];
         if ( (i>4) && (upv["delta"]==0)) {continue;}
         label = upv["label"];

         extra="";
         if (upv["flash"]) {
           divs_to_flash.push(id_name);
           extra = " background-color:white; ";
         }
         list = list + '<div class="quark_trending_divs" id="' + id_name + '" style="' + extra + ' transition:background-color 3s; position:relative; margin-left:5px; width:365px; border-radius:3px min-height:28px; margin-bottom:3px;">';
         list = list + starThis(upv["delta"]) + arrowThat(upv["upvotes"]) + '<a style="color:black;" target="_blank" href="' + urls + '"><div style="margin-left:32px; padding-top:10px; width:280px;">' + shortenString(label) + '</div> <div class="flash_prompt" style="display:none; position:absolute; left:130px; top:0px; font-size:10px; color:#666666">Just upvoted!</div></a>';

         list = list + "</div>";
      }

      

      xx = document.getElementById('qobalt_trending');
      xx.innerHTML = list;

     

      //display list
    
      
    } catch(err) {
      console.log(err);
    } 
    setTimeout(function(){addEvents();},2000);
      

   });

}

function tab2_processWait(count)
{
   //console.log("ProcessWait ...");
   count--;
    if (count<=0) {
           console.log("tab2_processWait : timing out.");
           g_cur_item++;
           setTimeout(tab2_processNextAnswer,TAB2_PAUSE_FOR_ANSWER);
           return;
    }
   console.log("tab_processWait : " + count);
   var store = chrome.storage.local.get(
    null,
    function(res){
       var recent_item = res["recent_update"];
       console.log("QQQQQQQQQQQQQQQQQQQQQQQQQQQ    Tab2 : recent_update = " + recent_item + "," + g_cur_url);
       //console.log("Peeking " + recent_item + " | against " + g_cur_url);
       if (typeof(recent_item)=='undefined') {
         recent_item="none";
       }

       if (!('upvotes' in res) || ((recent_item!=g_cur_url))) {
         setTimeout(function(){tab2_processWait(count);},TAB2_PAUSE_FOR_ANSWER);

       } else {
        
         // record ordinal
         // upvotes[url]
         console.log("~~~~~~~~~~~~~~~~~ " + JSON.stringify(res["upvotes"]));
         res["upvotes"][g_cur_url]["order"] = g_cur_item; // bloom
         res["progress_num"] = g_cur_item;
         chrome.storage.local.set(res);
         console.log('ZZZZZZZZZZZAAAAAAAAAAAAAAAAAAAAAAAAAA ' + g_cur_item);
         g_cur_item++;
         setTimeout(tab2_processNextAnswer,TAB2_PAUSE_FOR_ANSWER);
       }
    });
}

function tab2_processNextAnswer()
{
   try {
     
     if (g_cur_item == g_items.length) {
        var save = chrome.storage.local.set({
           "progress": "ready"
           }, 
        function(){
             chrome.runtime.sendMessage({name: "close_refresh"}); 
        });

       
        // close this window
        return;
     }

     g_cur_url = g_items[g_cur_item];
    
     sending = chrome.runtime.sendMessage(
        {name: "get_upvotes", url: window.location.protocol + "//" + window.location.host + g_cur_url},
        function(res){
           setTimeout(  function(){tab2_processWait(20);} ,200);
        }
     );  
    
  } catch(err) {
    console.log(err);
  }
 
}

function tab2_processAnswers()
{
  sending = chrome.runtime.sendMessage({name: "open_upvote_tab"});

  g_cur_item = 0;
  //g_cur_url = g_items[g_cur_item];
  setTimeout(tab2_processNextAnswer,50);

}

function tab2_getAnswersFromContent()
{

   count=0;
   chrome.storage.local.set({
           "progress_num": 0
   });
  
   var l_list = document.getElementsByClassName('pagedlist_item');
   var l_size = l_list.length;
   for (i=0; i<l_size; i++) {
           try {
              var dd = l_list.item(i);
              metaTxt = dd.getElementsByClassName('metadata')[0].innerHTML;
              if (metaTxt.indexOf('Added')>-1) {
                if (dd.innerHTML.indexOf('Your answer to')>-1) {
                  //console.log("ZEZ : " + dd.className);
                  alink = dd.querySelector('a');
                  dl = alink.getAttribute('href');
                  dt = skippast(metaTxt,'Added ');
                  

                  dl = decodeURI(dl);
                  
                 if (!g_items_set.has(dl)) {
                      
                      g_items.push(dl);
                      g_items_set.add(dl); 
                      if (g_items.length>=MAX_CONTENTS) {break;}
                      //g_timestamp[dl] = dt;              
                  }
                }
              } else {
                 //console.log(metaTxt);
              }
              
           } catch(err) {
             console.log(err);
           }
  } /*for*/

  tab2_processAnswers();
   return;  

}

function tab2_numAnswers()
{
   var l_list = document.getElementsByClassName('pagedlist_item');
 
   var l_size = l_list.length;
   count=0;
   for (i=0; i<l_size; i++) {
    
        var dd = l_list.item(i);
        metaTxt = dd.getElementsByClassName('metadata')[0].innerHTML;
        if (metaTxt.indexOf('Added')>-1) {
          if (dd.innerHTML.indexOf('Your answer to')>-1) {
             count++;
          }
        }
   }
   return count;
}



function tab3_scrollDown(count)
{
   
   console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP ' + count);
   if (count--<0) {return;}
   window.scrollBy(0,1500);
   setTimeout(function(){tab3_scrollDown(count),500});
}


function tab2_scrollDown()
{
  
   console.log('WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW ');
   if (tab2_numAnswers()>MAX_CONTENTS){
     tab2_getAnswersFromContent();
     return;
   }
   window.scrollBy(0,1500);
   setTimeout(function(){tab2_scrollDown(),500});
}


function tab2_processBackgroundTab()
{

  setTimeout(function() {tab2_scrollDown()}, 500);
  //setTimeout(function() {tab2_getAnswersFromContent();}, TAB2_WAIT_FOR_SCROLL);
}

function sParseInt(val)
{
   x = 0;
   try {
     x = parseInt(val);
   } catch(err) {
     x=0;
   }
   if (isNaN(x)){x=0;}
   return x;
}


function processUpvotes()
{


   var txt = document.body.innerHTML;
   pos = txt.indexOf(" Upvotes</span>");
   if (pos<0) {
     pos = txt.indexOf(" Upvote</span>");
   }
   txt = txt.substring(0, pos);

   txt = txt.substring(txt.lastIndexOf(">")+1);

   txt = txt.replace(",","");
   txt = sParseInt(txt);

   var url = decodeURI(window.location.pathname);
   //url = url.split("#")[0];
  
   
   var store = chrome.storage.local.get(null,function(res){
     try {
      // Just grab them all.
        var upvotes = res["upvotes"];

        if (typeof(upvotes)=='undefined') {
          upvotes={};
        }
        if (!(url in upvotes)) {
          console.log(url + " URL not found in snap.");
          upvotes[url] = {"upvotes":0, "timestamp":0, "delta":0, "label":""};
        }

        var label = document.getElementsByClassName('ui_qtext_rendered_qtext')[0].innerHTML;
        console.log("ZEZEZ " + label);

        var currentStamp = (new Date).getTime();
        currentStamp = currentStamp/1000/60; // to minutes.
 

        //var delta = txt - old_upvotes;
        var delta = 0;
        //console.log("Delta " + delta + " :::: old " + old_upvotes + " current - " + txt);

        if (txt!=upvotes[url]["upvotes"]) {
           console.log(url + " NOT found in snap.");
           recStamp = currentStamp;
        } else {
           console.log(url + " found in snap.");
           recStamp = upvotes[url]["timestamp"];
        }
        console.log("recstamp = " + recStamp);
          
        upvotes[url] = {upvotes: txt, timestamp: recStamp, label: label, delta: delta};
        console.log("Saved off upvotes[url]" + JSON.stringify(upvotes[url]));
        chrome.storage.local.set({
            "upvotes":upvotes
               //"upvotes": JSON.parse(JSON.stringify(upvotes)) 
        }, function(){
          chrome.storage.local.set({  
           "recent_update" : url
          });
        });
       
        
        
      
        
      } catch(err) {
        console.log(err);
      }

   });
 }
     
 

function loadNotifications()
{
   chrome.runtime.sendMessage({name: "open_notifs"});  
   setTimeout(function(){loadNotifications()},1000*60*NOTIFICATION_INTERVAL);
}

function tab3_pullNotificationsFromFeed()
{
  // get the first 7 elements matching - 

  match = ['commented on your answer to:', 'replied to your comment to:', 'answered:','Quora Digest:','followed you.'];
  results = [];
  //str2 = '';
  // replied to your comment to:
  // answered:
  // Quora Digest:
  // followed you.

  xx=document.getElementsByClassName('paged_list_wrapper');
  var htmlChildren = xx[0].children;
  var NUM_RESULTS=7;
  try {
    for (i=0; i<htmlChildren.length;i++) {
       if (results.length>=NUM_RESULTS) {
         break;
       }
       xx = htmlChildren[i];
       url = xx.getElementsByClassName('overlay')[0]['href'];
       buf = xx.innerText;
       res = {url: url, txt : buf};
       
       for (k=0; k<match.length; k++) {
         
         if (buf.indexOf(match[k])>0) {
            results.push(res);
            break;
         }
       }
    }
    
    var currentStamp = (new Date).getTime();
    currentStamp = currentStamp/1000/60; // to minutes.

    var setting = chrome.storage.local.set({"notifications": results, "notif_flag" : "display"},function(){
        if (FIREFOX) {
           chrome.runtime.sendMessage({name: "close_notifs"});
        } else {
           window.close();
        }

    });
    /*,
      function(){
        //console.log("PPPPP : " + JSON.stringify(results));
        chrome.runtime.sendMessage({name: "close_notifs"});
    });  
    */ 
    
  } catch(err) {
     console.log(err);
  } 
  
}

function surfaceNotifs()
{

   var dd = document.getElementById("qobalt_notifications");
   
   var buf = "";
   var bold_phrases = ["commented on", "Quora Digest", "were sent", "followed you","replied to your comment"];
   var store = chrome.storage.local.get(null, function(res) {
      var notifs = res["notifications"];

      for (i=0; i<notifs.length; i++) {
        buf = buf + "<a style='color:black;'' target='_blank' href='" + notifs[i]['url'] + "'><div style='margin-bottom:5px; width:350px;'>"
        for (j=0; j<bold_phrases.length;j++) {
          notifs[i]['txt'] = notifs[i]['txt'].replace(bold_phrases[j], '<b>' + bold_phrases[j] + '</b>');
        }
       
        buf = buf + notifs[i]['txt'];
        buf = buf + "</div></a>";
        buf = buf + "<div style='height:1px; margin-bottom:5px;background-color:#AAAAAA; width:150px;'>&nbsp;</div>";
      }
      dd.innerHTML = buf;
   });

   
}

function listenForNotifs()
{
  //zoomzoom
   store = chrome.storage.local.get(null,
     function(res) {
       var mode = res["notif_flag"];
       if (typeof(mode)=='undefined') {return;}
       if (mode=='display') {
         chrome.runtime.sendMessage({name: "close_notifs"});
         surfaceNotifs();
         chrome.storage.local.set({
          "notif_flag": "wait"
         }); 
       }
     });
  

}

function grabProfileLink()
{
  

}

function tab4_pullComments()
{
   comments = [];
   ndn = document.getElementsByClassName('CommentAddOperationView');
   for (i=0; i<3;i++) {
     try {
        xx = ndn[i];
        qtext = xx.previousSibling.innerText;
        txt=xx.getElementsByClassName('revision')[0].innerText;
        url = xx.getElementsByTagName('a')[0].href;
        comments.push({txt: txt, url: url, question: qtext});
     } catch(err) {
       ;
     }
      
   }

   storage = chrome.storage.local.set({
        "comments": comments
   }, function(){
     if (FIREFOX) {
       chrome.runtime.sendMessage({name: "close_comment_tab"}); 
     } else {
        window.close();
     }
   });

}

function tab4_scrollDown(count)
{
   if (count--<0) {tab4_pullComments(); return;}
   window.scrollBy(0,1500);
   setTimeout(function(){tab4_scrollDown(count),500});
}

function forceUpdate()
{
   g_warp_refreshTimer=true;
}

function offMainPage()
{  

   store = chrome.storage.local.get(null,function(res){
     g_background_color = res["theme_color"];
     if (typeof(g_background_color)=='undefined') {g_background_color='#CCCCCC';}
     console.log('=========================' + g_background_color);
     var classic = res["classic_view"];
     if (typeof(classic) == 'undefined') {classic = false;}
     if (!classic) {
       newStyle='';
       newStyle +=  "body{background-color: #CCCCCC !important; font-family: 'Lato', sans-serif;font-weight: 400;} #center{width:70%;margin-left:auto;margin-right:auto;} #tip{background-color:#bbbbbb;padding:4px;margin-top:80px;width:180px;float:right;float:top;font-size:80%;border-radius:3px;} hr{color:#cccccc;display:none;}";
       //newStyle += ".LoggedInSiteHeader{background-color:#aaaaaa};";
       newStyle += '.SiteHeader {background-color: #DDDDDD !important;}';
       newStyle += '.SiteHeader .selector_input {background-color:white !important;}';
       newStyle += '.ask_bar .selector_input {background-color: white !important;}';
       newStyle += '.QuestionStats {position: absolute !important; top: 70px !important;}';
       newStyle += '.RelatedQuestions .more_link {display: none !important;}';

       newStyle += '.question_related.list{position:absolute !important; top:400px !important; padding:10px !important;width:350px; font-size:12px !important; background-color:#DDDDDD !important;}';

       newStyle += '.QuestionStats a {color: #2b6dad !important;}';
       newStyle += '.QuestionStats {color: black !important;}';
       newStyle += '.ContentPageFeed {background: #DDDDDD !important;}';
       newStyle += '.Answer {background-color: white;border-radius: 5px; margin-bottom:20px;};'
       newStyle += '.inline_editor_content {all: initial; * {all: unset;}}';
       newStyle += '.inline_editor_content {margin-bottom: 10px; padding-left:10px !important; padding: 10px !important; margin-bottom: 10px !important;}';
       newStyle += '.inline_editor_value {all: initial; * {all: unset;}}';
       newStyle += '.inline_editor_value {margin-bottom: 10px; padding-left:10px !important; padding: 10px !important; margin-bottom: 10px !important;}';

       //newStyle += '.Answer {background-color: white;border-radius: 5px;margin-bottom: 10px; padding: 10px !important; padding-top:50px !important;}';
       //newStyle += '.Answer .Editor {background-color: white;border-radius: 5px;margin-bottom: 10px; padding-top:50px !important; padding: 10px !important;}';
       
       newStyle += '.related_question .ui_qtext_rendered_qtext {font-size: 12px !important;}';
       newStyle += '.qtext_hr {display: block !important;}';

       newStyle += '#loadOverlay{display: none;}';
       newStyle = "<style>" + newStyle + "</style>";
       document.head.insertAdjacentHTML( 'afterbegin', newStyle);
     }
    });

}

function afterVersionCheck()
{
   loadNotifications(); // 
   setInterval(function(){listenForNotifs();},1000*4);
   surfaceFeedUpvotes(true /*right now*/);
   timeOfLastUpdateThen();
}


function checkVersion()
{


  var store = chrome.storage.local.get(null,function(res){
       if ('qobalt_version' in res) {
       if (res['qobalt_version']==QOBALT_VERSION) {
         console.log("Normal startup.");
         afterVersionCheck();
         return;
       }
     }
     
     clearing = chrome.storage.local.clear(function(){
       res = {qobalt_version: QOBALT_VERSION};
        chrome.storage.local.set(res);
        // no need, clearing snaps takes care of refresh
        //iconHelp(false);
        console.log("Fresh startup.")
        afterVersionCheck();

     });

  });
  
 
}

//https://gist.github.com/mathiasbynens/428626

document.head || (document.head = document.getElementsByTagName('head')[0]);

function changeFavicon(src) {
 var link = document.createElement('link'),
     oldLink = document.getElementById('dynamic-favicon');
 link.id = 'dynamic-favicon';
 link.rel = 'shortcut icon';
 link.href = src;
 if (oldLink) {
  document.head.removeChild(oldLink);
 }
 document.head.appendChild(link);
 document.title = "Qobalt temp tab ..."
}


(function() { 
 try {
   FIREFOX = ( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 );

   window.qlinks_seen = {};
   // This is my Stoyle!   Are you impressed by my Stoyle!   What is your Stoyle !?
   //newStyle = "<head><style> .side_bar{background-color:#c0c0c0 !important} .LoggedInSiteHeader{background-color:#aaaaaa !important;} body {background-color:#cccccc !important;} </style></head>";
   //document.body.insertAdjacentHTML( 'afterbegin', newStyle);


   var url = window.location.href;


   if (url.indexOf('qobalt')>0) {
     if (url.indexOf('get_upvotes')>0) {
        if (!(FIREFOX)) {changeFavicon(chrome.extension.getURL("images/qobalt_16.jpg") );}
        processUpvotes();
        return;
     } else if (url.indexOf('notifications')>0) {
        if (!(FIREFOX)) {changeFavicon(chrome.extension.getURL("images/qobalt_16.jpg") );}
        tab3_scrollDown(5);
        setTimeout(function(){tab3_pullNotificationsFromFeed();},4000);
        return;
     } else if (url.indexOf('qobalt_comments')>0) {
        if (!(FIREFOX)) {changeFavicon(chrome.extension.getURL("images/qobalt_16.jpg") );}
        tab4_scrollDown(5); // see timeout for rest of processing
     } else  if (url.indexOf('qobalt_answers')>0) {
       if (!(FIREFOX)) {changeFavicon(chrome.extension.getURL("images/qobalt_16.jpg") );}
       tab2_processBackgroundTab();
       return;
     }
   }


   if (url.substr(url.indexOf('quora.com/'))!='quora.com/') {
     offMainPage();
     return;
   }

   if (FAST_MODE!="production") {
      alert("Warning - Fast mode is set to " + FAST_MODE);
   }

   var timer = null;

  try {
     
     xx = document.getElementsByClassName('FeedAddQuestionPrompt')[0];
     gg = xx.getElementsByTagName('a')[0];
     g_profile_link = gg.href;
     chrome.storage.local.set({
           "profile_link": gg.href
     },function(){
       chrome.runtime.sendMessage({name: "initialize"});   
       panelSkeleton();
       checkVersion();
     });
    
   } catch(err) {
      console.log(err);
   }

   newStyle = '';
   store = chrome.storage.local.get(null,
    function(res)
    {
       var mode = res["mode"];
       var anon = res["anon"];
       var link_color = res["link_color"];
       g_anon_color = res["anon_color"];
       g_background_color = res["theme_color"]
       var classic = res["classic_view"];

       if (typeof(classic) == 'undefined') {classic = false;}
       if (typeof(mode) == 'undefined') {mode='hightlight';}
       if (typeof(anon) == 'undefined') {anon='highlight';}

       if (typeof(g_background_color) == 'undefined') {g_background_color = '#cccccc';}
       if (typeof(link_color)=='undefined') {link_color='#ffffcc';}
       if (typeof(g_anon_color)=='undefined') {g_anon_color='#e7f4e6';}

       

       try {
          chrome.runtime.sendMessage({name: "banner_already_shown"}, function(response) {
            //console.log(response.status);
            if ( !response.status && (mode != 'chill')) {
               showBanner();
            }
          });
        } catch(err) {
          ;
        }
       
       if (mode == 'chill') {
         newStyle='';
       } else if (mode == 'suppressed') {
         //showBanner();
         newStyle = '.HyperLinkFeedStory {display: none !important; height:0px !important;}';
       } else {
         //showBanner();
         newStyle = '.HyperLinkFeedStory {background-color: ' + link_color + ' !important;}';
       }
       //aa=document.getElementsByTagName('textarea');
       //aa[0].style.backgroundColor='white';
   
       //newStyle += ".LoggedInSiteHeader{background-color:#aaaaaa};";
       //newStyle += ".Toggle {background-color:green !important;}";
       //.RelatedQuestions {display: none !important;}
       //.QuestionStats:link {color: white;}
       //.QuestionStats a, .answer_draft_side_col a {color: white;}
       // Somebody ... brought a knife to a gun fight.
       newStyle += '.ui_layout_content_wrapper {all: initial; * {all: unset;}}';
       newStyle += '.u-relative {all: initial; * {all: unset;}}';
       newStyle += '.u-width--100 {all: initial; * {all: unset;}} ';
       newStyle += '.u-margin-right--sm {all: initial; * {all: unset;}} ';
       newStyle += '.ui_qtext_truncated_text {all: initial; * {all: unset;}} ';
       newStyle += '.ui_qtext_truncate_4 {all: initial; * {all: unset;}} ';
       newStyle += '.u-bg--cover {all: initial; * {all: unset;}}';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {all: initial; * {all: unset;}}';
       //newStyle += '.ui_layout_thumbnail {all: initial; * {all: unset;}'};

       newStyle += '.header_image_with_site_header {display:none;}';
       newStyle += '.ui_layout_content_wrapper {width:100% !important;}';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {display:none !important;}';

       //newStyle += '.ui_qtext_rendered_qtext {width:572px !important}';

       //newStyle += '.ui_qtext_truncated_text {all: initial; * {all: unset;}} ';
       //newStyle += '.ui_qtext_rendered_qtext {font-size:15px; font-weight:400;}';    
       newStyle += '.ui_story_title ui_story_title_medium.ui_qtext_rendered_qtext {font-size: 18px; font-weight:700 !important}'; 

       newStyle += '.ui_qtext_truncated_text {font-size:15px !important; font-weight:400 !important; font-family: q_serif,Georgia,Times,"Times New Roman","Hiragino Kaku Gothic Pro","Meiryo",serif !important;}';
       newStyle += '.ui_qtext_truncated {font-size:15px !important; font-weight:400 !important; font-family: q_serif,Georgia,Times,"Times New Roman","Hiragino Kaku Gothic Pro","Meiryo",serif !important;}';
       newStyle += '.ui_header_image_wrapper {display:none !important};';
       newStyle += '.u-bg--cover {height:0px !important; width:0px !important; display:none !important;}';
       newStyle += '.u-width--100 {width:100% !important};';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {width: 0px !important; display:none !important;}';
       newStyle += '.feed_card_on .Bundle .full_bundle {border: 1px solid #efefef !important; border-bottom: 0px;}';
       newStyle += '.sticky_bundle_wrapper {display: none;}';

       newStyle += '.InteractionModeBanner{display:none !important;}';

       
       //newStyle += '.ContentWrapper {margin:20px}';

       //newStyle += '.layout_3col_left .EditableList{width:50px;}';
       //newStyle += '.layout_3col_left .PagedList {display: none;}';
       //newStyle += '.layout_3col_left .edit_link {display: none;}';

       //newStyle += '.layout_3col_left {display:block;}';
       //newStyle += '.HomeMultifeed {left: 40px !important;}';
       
       newStyle += '.EquivalentQuestionsInOtherNetworks {display: none !important;}';
       newStyle += '.Toggle.SimpleToggle.RelatedQuestions {height: 0px !important;}';
       newStyle += '.qtext_hr {display: block !important;}';
       newStyle += '.grid_page {margin-left: -100px;}';
       newStyle += '.layout_3col_right .fixable_clone {font-size:11px; background-color: #cccccc !important; height: 100% !important; width: 380px; margin: 0px;}';
       
        // Your gay friend stops you from leaving the house looking like that.
       if (!classic) {
         newStyle +=  "body{background-color: " + g_background_color + " !important; font-family: 'Lato', sans-serif;font-weight: 400;} #center{width:70%;margin-left:auto;margin-right:auto;} #tip{background-color:#bbbbbb;padding:4px;margin-top:80px;width:180px;float:right;float:top;font-size:80%;border-radius:3px;} hr{color:#cccccc;display:none;}";
         newStyle += '.SiteHeader {background-color: #DDDDDD !important;}';
         newStyle += '.SiteHeader .selector_input {background-color:white !important;}';
       }


       newStyle = '<style>' + newStyle + '</style>';
    
       
       document.head.insertAdjacentHTML( 'beforeend', newStyle);

      
       console.log('---------------ZZZ');


       
       // zoom
       if (anon != 'show') {
           window.anon = anon;
           scanForAnonQ();        
           //https://stackoverflow.com/questions/4620906/how-do-i-know-when-ive-stopped-scrolling-javascript
           window.addEventListener('scroll', function() {
                if(timer !== null) {
                    clearTimeout(timer);        
                }
                timer = setTimeout(function() {
                      scanForAnonQ();
                }, 150);
           }, false);
        }     
    });
} catch(err) {
  console.log(err);
}

})();

//.................................................................
// Injected code accessed by elements in page itself.
