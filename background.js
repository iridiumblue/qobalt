
var FIREFOX = ( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 );


var bannerAlreadyShown=false;
var contentLoaded = false;
//var backTab = -1;

function loadRecent(tab) 
{
    
}

function onCreated(tab) 
{
  contentLoaded = true;
  loadRecent(tab);
  //backTab = tab.id;
  console.log(`Created new tab: ${tab.id}`)
}

function onError(error) {
  console.log(`Error: ${error}`);
}

function releaseTab(tabid)
{
  var store = chrome.storage.local.get(null,function(res){
     if ('open_tabs' in res) {
       idx=res['open_tabs'].indexOf(tabid);
       if (idx > -1) {
          res['open_tabs'].splice(idx, 1);
       }
    } 
    chrome.storage.local.set(res);  
  });

}


function closeStaleTabs()
{
  console.log('-------######################------ here');
     chrome.storage.local.get(null,function(res){
     console.log('-------######################------ here2');
      for (i=0; i<res['open_tabs'].length;i++) {
         try {
            chrome.tabs.remove(res['open_tabs'][i]);
          } catch(err) {console.log(err);}
         
      }
      res['open_tabs']=[]; 
      chrome.storage.local.set(res);
    });
}

function trackNewTab(id)
{
  chrome.storage.local.get(null,function(res){
      if (!('open_tabs' in res)) {
         res['open_tabs']=[];
      }
      res['open_tabs'].push(id);
      chrome.storage.local.set(res);
  });
 
}

function unTrackTab(id)
{
   chrome.storage.local.get(null,function(res){
      if (!('open_tabs' in res)) {
        return;
      }
      idx = res['open_tabs'].indexOf(id);
      if (idx > -1) {
         res['open_tabs'].splice(idx, 1);
      }
      chrome.storage.local.set(res);

   });
}


function handleClick() {
  chrome.runtime.openOptionsPage();
}


var g_content_tab=0;
var g_upvote_tab=0;
var g_notif_tab=0;
var g_comment_tab=0;

var g_main_tab_id='empty';

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  console.log("Background ::: " + request.name);


  if (request.name == "initialize") {
     //... There can be only one main window
     if ((g_main_tab_id=='empty') || (g_main_tab_id==sender.tab.id)) {
        closeStaleTabs();
        g_main_tab_id=sender.tab.id;
        console.log("!!!!!!!!!!!!!!!!!!!!!!!!! Main tab id = " + g_main_tab_id);
     } else {
        chrome.tabs.remove(g_main_tab_id);
        chrome.tabs.remove(g_content_tab); g_content_tab=0;
        chrome.tabs.remove(g_upvote_tab); g_upvote_tab=0;
        chrome.tabs.remove(g_notif_tab); g_notif_tab=0;
        g_main_tab_id = sender.tab.id;
     }
     
  }
  
  if (request.name == "refresh_stats") {
    // detect if main window has changed
    if (g_main_tab_id=='empty') {g_main_tab_id = sender.tab.id;}
    else if (g_main_tab_id!=sender.tab.id) {return;}
    var creating = chrome.tabs.create({
      url:"https://www.quora.com/content?content_types=answers#qobalt_answers",
      active:false
    }, function(tab){
      trackNewTab(tab.id);
      if (FIREFOX) {chrome.tabs.hide(tab.id); }
      g_content_tab=tab.id;
    });

  };
  // bail out of everything else if not main tab
  if (sender.tab.id==g_main_tab_id) {

    if (request.name=="open_notifs") {
      var creating = chrome.tabs.create({
        url:"http://www.quora.com/notifications#qobalt",
        active:false
        
      }, function(tab){
        trackNewTab(tab.id); 
        if (FIREFOX) {chrome.tabs.hide(tab.id);}
        g_notif_tab=tab.id;

      });
      
    };
  }

  if (request.name=="close_notifs") {
     try{
       chrome.tabs.remove(g_notif_tab);
      } catch(err){console.log(err);}
     releaseTab(g_notif_tab);
  }

  if (request.name == "close_refresh") {
     try {
       chrome.tabs.remove(g_content_tab);
     } catch(err){console.log(err);}
     try {
      chrome.tabs.remove(g_upvote_tab);
     } catch(err){console.log(err);}
     releaseTab(g_content_tab);
     releaseTab(g_upvote_tab);
     console.log("REMOVING TABS " + g_content_tab + "," + g_upvote_tab);
  };

  if (request.name == "open_comment_tab") {
      store = chrome.storage.local.get(null,function(res){
         profLink = res["profile_link"];
         logLink = profLink+"/log#qobalt_comments";
         var creating = chrome.tabs.create({
            url:logLink,
            active:false
         }, function(tab){
          trackNewTab(tab.id);
           if (FIREFOX) { chrome.tabs.hide(tab.id);}
           g_comment_tab=tab.id;
         });

      });

     
  };

  if (request.name == "close_comment_tab") {
    try{
       chrome.tabs.remove(g_comment_tab);
     } catch(err){console.log(err);}
     releaseTab(g_comment_tab);
  };
  
  if (request.name == "open_upvote_tab") {
    var creating = chrome.tabs.create({
      url:"http://captive.apple.com/#qobalt_answers",
      active:false
    }, function(tab){
      trackNewTab(tab.id);
       if (FIREFOX) { chrome.tabs.hide(tab.id);}
       g_upvote_tab=tab.id; console.log("SCROLLING Tab ID " + g_upvote_tab);

    });   
  };
  
  
  if (request.name == "get_upvotes") {
     var updating = chrome.tabs.update(
       g_upvote_tab,              // optional integer
       {url: request.url + "#qobalt_get_upvotes"}
     )
  };

    //..
  if (request.name == "banner_already_shown") {

      sendResponse({status: bannerAlreadyShown});
      bannerAlreadyShown=true;
  }
  
});



chrome.tabs.onRemoved.addListener(
    function(tabId, removeInfo){
      if (tabId==g_main_tab_id) {
         closeStaleTabs();
         g_main_tab_id='empty';
      }
      console.log('Clearing main tab');
    });


chrome.browserAction.onClicked.addListener(handleClick);

//... if version upgrade then clear local storage.



